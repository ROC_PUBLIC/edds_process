#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import datetime
from tempfile import tempdir
from pathlib import Path

import pytest

from edds_process.response.parse import parse_tcreport_xml, \
    parse_raw_xml, xml_to_dict, remove_scos_header, \
    count_packets
from edds_process.response.make import make_tmraw_xml, make_tcreport_xml, make_param_xml

from .constants import BASE_DIR


@pytest.mark.parametrize('as_list', [False, True])
def test_response_xml_to_dict(as_list):
    """
    Test response.xml_to_dict method

    :return:
    """
    if as_list:
        expected_type = list
    else:
        expected_type = dict
    xml_file = os.path.join(BASE_DIR, 'BatchRequest.PktTcReport.SOL.Example.xml')
    assert isinstance(xml_to_dict(xml_file, as_list=as_list), expected_type)


def test_response_remove_scos_header():
    """
    Test response.remove_scos_header()

    :return:
    """
    inputs = '000000005ccaa76d0009f7505ccaa76d0009b19200000001028a00ca00000064183503e8000000000000d6f600000000028a0000000000010000000000300000ffffffffff1004b7000005010cb7c000001110050100800000000020a6cd010901030000'
    expected_outputs = '0CB7C000001110050100800000000020A6CD010901030000'
    assert remove_scos_header(inputs) == expected_outputs


def test_response_parse_tcreport_xml():
    """
    Test response.parse_tcreport_xml function

    :return:
    """

    # Expected output dict
    expected = {('ZIW00015',
                 datetime.datetime(2019, 5, 2, 8, 17, 15, 834990)): {'RawBodyData': '1CBCC323000519B407789691',
                                                                     'OnBoardAccState': 'PASSED',
                                                                     'ExecCompState': 'PASSED',
                                                                     'ExecCompPBState': 'UNKNOWN',
                                                                     'SequenceName': 'AIWF011A',
                                                                     'uniqueID': 'SRPWP76DB80690C00000',
                                                                     'Description': 'DPU_ENABLE_WATCHDOG',
                                                                     'ReleaseTime': '2019-05-02T08:17:15.539567Z',
                                                                     'UplinkTime': '2019-05-02T08:17:15.834990Z',
                                                                     'ReleaseState': 'PASSED',
                                                                     'GroundState': 'PASSED',
                                                                     'UplinkState': 'PASSED',
                                                                     'OnBoardState': 'PASSED',
                                                                     'Apid': '1212'},
                ('ZIW00012', datetime.datetime(2019, 5, 2, 8, 26, 16, 705419)): {
                    'RawBodyData': '1CBCC324000519110178731F',
                    'OnBoardAccState': 'PASSED',
                    'ExecCompState': 'PASSED',
                    'ExecCompPBState': 'UNKNOWN',
                    'SequenceName': 'AIWF370A',
                    'uniqueID': 'SRPWP76DB80690C00001',
                    'Description': 'DPU_TEST_CONNECTION',
                    'ReleaseTime': '2019-05-02T08:26:15.286624Z',
                    'UplinkTime': '2019-05-02T08:26:16.705419Z',
                    'ReleaseState': 'PASSED',
                    'GroundState': 'PASSED',
                    'UplinkState': 'PASSED',
                    'OnBoardState': 'PASSED',
                    'Apid': '1212'}}

    tcreport_xml = os.path.join(BASE_DIR, 'BatchRequest.PktTcReport.SOL.Example.xml')

    tcreport = parse_tcreport_xml(tcreport_xml)

    assert isinstance(tcreport, dict)
    assert tcreport == expected


def test_response_parse_raw_xml_tmraw():
    """
    Test response.parse_raw_xml function with TmRaw DDS response XML file

    :return:
    """

    expected = [
        '000000005ccaa76d0009f7505ccaa76d0009b19200000001028a00ca00000064183503e8000000000000d6f600000000028a0000000000010000000000300000ffffffffff1004b7000005010cb7c000001110050100800000000020a6cd010901030000',
        '000000005ccaa76d0009f7515ccaa76d0009b83700000001028a00ca00000060183503e8000000000000d6f700000000028a0000000000010000000000300000ffffffffff1004b7000105010cb7c001000d10050100800000000040a6d80100']

    raw_xml = os.path.join(BASE_DIR, 'BatchRequest.PktTmRaw.SOL.Example.xml')

    raw = parse_raw_xml(raw_xml)

    assert isinstance(raw, list)
    assert raw == expected


def test_response_parse_raw_xml_tcraw():
    """
    Test response.parse_raw_xml function with TcRaw DDS response XML file

    :return:
    """

    expected = [
        '000001005ccb50a4000c72be5ccb5c12000c941600019a57028a00ca000000c8283000010000cd2b000000c800000000028a0000000000010000000000000000000000005ccb5c12000c94165ccb627a0007f6510000247f05b8006003fc05b80602090063060160c8c800030009000000741a7f0000c0cfe0cf5ccb51900009000363741a7f0000c0cfe0cf5ccb5190000a0000000000000000c0cfe0cf5ccb51900000000224c35100000000000000000000003c000026c35200000000000000000000003c0000',
        '000001005ccb67a4000262b95ccb67a4000262b900019b75028a00ca000000ec283000010000cdba000000c800000000028a0000000000010000000000000000000000005ccb67a4000262b95ccb758a000c7022000078960001008403fc0455038809011e000160c8c80005000900e268761a7f0000d0cfe0cf5ccb67ea000900e268761a7f0000e0cfe0cf5ccb67ea000900e268761a7f0000e0cfe0cf5ccb67ea000900e268761a7f0000d0cfe0cf5ccb67ea000a0000000000000000d0cfe0cf5ccb67ea0000000224c35100000000000000000000003c000026c35200000000000000000000003c0000'
    ]

    raw_xml = os.path.join(BASE_DIR, 'BatchRequest.PktTcRaw.SOL.Example.xml')

    raw = parse_raw_xml(raw_xml)

    assert isinstance(raw, list)
    assert raw == expected


@pytest.mark.parametrize('edds_xml, file_type', [
    (os.path.join(BASE_DIR, 'BatchRequest.PktTmRaw.SOL.Example.xml'), None),
    (os.path.join(BASE_DIR, 'BatchRequest.PktTcReport.SOL.Example.xml'), None),
    (os.path.join(BASE_DIR, 'BatchRequest.PktTmRaw.SOL.Example.xml'), 'TmRaw'),
    ('invalid_xml_file', None),
    ('invalid_xml_file', 'WrongType'),
])
def test_response_count_packets(edds_xml, file_type):
    """
    Test response.count_packets()

    :return:
    """

    packet_count = count_packets(edds_xml, file_type=file_type)

    if edds_xml != 'invalid_xml_file':
        assert isinstance(packet_count, int)
        assert packet_count == 2
    else:
        if file_type == 'WrongType':
            assert packet_count == []
        else:
            assert packet_count is None


@pytest.mark.parametrize('overwrite, already_exists', [
    (False, False),
    (False, True),
    (True, True),
    (True, False),
])
def test_response_make_tmraw_xml(overwrite, already_exists):
    """
    Test response.make_tmraw_xml()

    :return:
    """

    input_tmraw = [
        '000000005ccaa76d0009f7505ccaa76d0009b19200000001028a00ca00000064183503e8000000000000d6f600000000028a0000000000010000000000300000ffffffffff1004b7000005010cb7c000001110050100800000000020a6cd010901030000',
        '000000005ccaa76d0009f7515ccaa76d0009b83700000001028a00ca00000060183503e8000000000000d6f700000000028a0000000000010000000000300000ffffffffff1004b7000105010cb7c001000d10050100800000000040a6d80100']
    output_file = Path(tempdir) / 'test_response_make_tmraw_xml.xml'
    if not overwrite and not already_exists:
        # Case where output file does not exist, and overwrite is False
        output_file.unlink(missing_ok=True)
        is_created = make_tmraw_xml(input_tmraw, output_file,
                                    overwrite=overwrite)
        assert is_created
    elif not overwrite and already_exists:
        # Case where file already exists, but overwrite is False
        output_file.touch(exist_ok=True)
        is_created = make_tmraw_xml(input_tmraw, output_file,
                                    overwrite=overwrite)
        assert is_created == output_file
    elif overwrite and already_exists:
        # Case where file already exists and overwrite is True
        output_file.touch(exist_ok=True)
        is_created = make_tmraw_xml(input_tmraw, output_file,
                                    overwrite=overwrite)
        assert is_created
    elif overwrite and not already_exists:
        # Case where file already exists and overwrite is True
        output_file.unlink(missing_ok=True)
        is_created = make_tmraw_xml(input_tmraw, output_file,
                                    overwrite=overwrite)
        assert is_created

    # Test has succeeded, then remove temporary test file(s)
    output_file.unlink(missing_ok=True)


@pytest.mark.parametrize('overwrite, already_exists', [
    (False, False),
    (False, True),
    (True, True),
    (True, False),
])
def test_response_make_tcreport_xml(overwrite, already_exists):
    """
    Test response.make_tcreport_xml()

    :return:
    """

    # Expected output dict
    input_tcreport = [{'CommandName': 'ZIW00015',
                       'RawBodyData': '1CBCC323000519B407789691',
                       'OnBoardAccState': 'PASSED',
                       'ExecCompState': 'PASSED',
                       'ExecCompPBState': 'UNKNOWN',
                       'SequenceName': 'AIWF011A',
                       'uniqueID': 'SRPWP76DB80690C00000',
                       'Description': 'DPU_ENABLE_WATCHDOG',
                       'ExecutionTime': '2019-05-02T08:17:15.834990Z',
                       'ReleaseTime': '2019-05-02T08:17:15.539567Z',
                       'UplinkTime': '2019-05-02T08:17:15.834990Z',
                       'ReleaseState': 'PASSED',
                       'GroundState': 'PASSED',
                       'UplinkState': 'PASSED',
                       'OnBoardState': 'PASSED',
                       'Apid': '1212'},
                      {
                          'CommandName': 'ZIW00012',
                          'RawBodyData': '1CBCC324000519110178731F',
                          'OnBoardAccState': 'PASSED',
                          'ExecCompState': 'PASSED',
                          'ExecCompPBState': 'UNKNOWN',
                          'SequenceName': 'AIWF370A',
                          'uniqueID': 'SRPWP76DB80690C00001',
                          'Description': 'DPU_TEST_CONNECTION',
                          'ExecutionTime': '2019-05-02T08:26:16.705419Z',
                          'ReleaseTime': '2019-05-02T08:26:15.286624Z',
                          'UplinkTime': '2019-05-02T08:26:16.705419Z',
                          'ReleaseState': 'PASSED',
                          'GroundState': 'PASSED',
                          'UplinkState': 'PASSED',
                          'OnBoardState': 'PASSED',
                          'Apid': '1212'}]

    output_file = Path(tempdir) / 'test_response_make_tcreport_xml.xml'
    if not overwrite and not already_exists:
        # Case where output file does not exist, and overwrite is False
        output_file.unlink(missing_ok=True)
        is_created = make_tcreport_xml(input_tcreport, output_file,
                                       overwrite=overwrite)
        assert is_created
    elif not overwrite and already_exists:
        # Case where file already exists, but overwrite is False
        output_file.touch(exist_ok=True)
        is_created = make_tcreport_xml(input_tcreport, output_file,
                                       overwrite=overwrite)
        assert is_created == output_file
    elif overwrite and already_exists:
        # Case where file already exists and overwrite is True
        output_file.touch(exist_ok=True)
        is_created = make_tcreport_xml(input_tcreport, output_file,
                                       overwrite=overwrite)
        assert is_created
    elif overwrite and not already_exists:
        # Case where file already exists and overwrite is True
        output_file.unlink(missing_ok=True)
        is_created = make_tcreport_xml(input_tcreport, output_file,
                                       overwrite=overwrite)
        assert is_created

    # Test has succeeded, then remove temporary test file(s)
    output_file.unlink(missing_ok=True)


@pytest.mark.parametrize('overwrite, already_exists', [
    (False, False),
    (False, True),
    (True, True),
    (True, False),
])
def test_response_make_param_xml(overwrite, already_exists):
    """
    Test response_make_param_xml()

    :param overwrite:
    :param already_exists:
    :return:
    """

    input_param_list = [{'Name': 'NCAT1220', 'TimeStampAsciiA': '2022-10-22T00:00:03.410972', 'Unit': 'none',
                         'Description': 'THR 6A Cumulative OnTime', 'EngineeringValue': '14.6367388038412',
                         'RawValue': '14.636738803841173'},
                        {'Name': 'NCAT11Y0', 'TimeStampAsciiA': '2022-10-22T00:00:03.410972', 'Unit': 'none',
                         'Description': 'THR 2A Cumulative OnTime', 'EngineeringValue': '9.61179027207941',
                         'RawValue': '9.611790272079407'},
                        {'Name': 'NCAT1270', 'TimeStampAsciiA': '2022-10-22T00:00:03.410972', 'Unit': 'none',
                         'Description': 'THR 2B Cumulative OnTime', 'EngineeringValue': '496.722078528859',
                         'RawValue': '496.72207852885873'}]

    output_file = Path(tempdir) / 'test_response_make_tcreport_xml.xml'
    if not overwrite and not already_exists:
        # Case where output file does not exist, and overwrite is False
        output_file.unlink(missing_ok=True)
        is_created = make_param_xml(input_param_list, output_file,
                                    overwrite=overwrite)
        assert is_created
    elif not overwrite and already_exists:
        # Case where file already exists, but overwrite is False
        output_file.touch(exist_ok=True)
        is_created = make_param_xml(input_param_list, output_file,
                                    overwrite=overwrite)
        assert is_created == output_file
    elif overwrite and already_exists:
        # Case where file already exists and overwrite is True
        output_file.touch(exist_ok=True)
        is_created = make_param_xml(input_param_list, output_file,
                                    overwrite=overwrite)
        assert is_created
    elif overwrite and not already_exists:
        # Case where file already exists and overwrite is True
        output_file.unlink(missing_ok=True)
        is_created = make_param_xml(input_param_list, output_file,
                                    overwrite=overwrite)
        assert is_created

    # Test has succeeded, then remove temporary test file(s)
    output_file.unlink(missing_ok=True)
